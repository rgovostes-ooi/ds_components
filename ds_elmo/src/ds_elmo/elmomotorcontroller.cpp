/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_elmo/elmomotorcontroller.h"
#include "elmomotorcontroller_private.h"

#include "ds_core_msgs/RawData.h"

namespace ds_elmo
{
ElmoMotorController::ElmoMotorController()
  : DsProcess(), d_ptr_(std::unique_ptr<ElmoMotorControllerPrivate>(new ElmoMotorControllerPrivate))
{
}

ElmoMotorController::ElmoMotorController(int argc, char** argv, const std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<ElmoMotorControllerPrivate>(new ElmoMotorControllerPrivate))
{
}

ElmoMotorController::~ElmoMotorController() = default;

void ElmoMotorController::setupConnections()
{
  DS_D(ElmoMotorController);
  d->elmo_sm_ = addIoSM("elmo_sm", "elmo");
  if (!d->elmo_sm_)
  {
    ROS_FATAL("Unable to create elmo state machine (no connection parameter namespace named 'elmo')");
    ROS_BREAK();
  }

  if (!d->elmo_sm_->getConnection())
  {
    ROS_FATAL("Unable to create elmo state machine connection (no connection parameter namespace named 'elmo')");
    ROS_BREAK();
  }
}

void ElmoMotorController::setupCommands()
{
  DS_D(ElmoMotorController);

  // Always disable motor output and echo.  Put these at the absolute beginning of the list
  d->init_commands_.emplace_front("MO=0\r", 0.2, false);
  d->init_commands_.front().setDelayAfter(ros::Duration(0.2));
  d->init_commands_.emplace_front("EO=1\r", 0.2, false);
  d->init_commands_.front().setDelayAfter(ros::Duration(0.2));

  for (auto& cmd : ros::param::param<std::vector<std::string>>("~init_commands", {}))
  {
    d->init_commands_.emplace_back(cmd, 0.2, false);
    d->init_commands_.back().setDelayAfter(ros::Duration(0.2));
  }

  for (auto& cmd : ros::param::param<std::vector<std::string>>("~poll_commands", {}))
  {
    const auto io_command = ds_asio::IoCommand{
      cmd, 0.2, false,
    };
    d->poll_commands_.emplace_back(std::make_pair(0, std::move(io_command)));
    d->poll_commands_.back().second.setDelayAfter(ros::Duration(0.2));
  }
}

void ElmoMotorController::setup()
{
  DsProcess::setup();
  setupCommands();

  DS_D(ElmoMotorController);

  // Now add the init commands as preempt commands
  for (auto& io_command : d->init_commands_)
  {
    io_command = d->wrap_command_for_logging(std::move(io_command));
    d->elmo_sm_->addPreemptCommand(io_command);
    ROS_INFO_STREAM("Added init command: " << io_command.getCommand());
  }

  // And the polled commands as regular commands.  Save the command
  // id after adding.
  for (auto& poll_command : d->poll_commands_)
  {
    poll_command.second = d->wrap_command_for_logging(std::move(poll_command.second));
    poll_command.first = d->elmo_sm_->addRegularCommand(poll_command.second);
    ROS_INFO_STREAM("Added poll command: " << poll_command.second.getCommand() << " (id=" << poll_command.first << ")");
  }

  d->has_started_ = true;
}

ds_asio::IoSM* ElmoMotorController::elmoStateMachine()
{
  DS_D(ElmoMotorController);
  return d->elmo_sm_.get();
}

ElmoMotorController::TimeStampedParam ElmoMotorController::parameter(const std::string& name)
{
  DS_D(ElmoMotorController);
  try
  {
    return d->parameters_.at(name);
  }
  catch (std::out_of_range& e)
  {
    ROS_WARN_STREAM("No value recorded for param '" << name << "'");
    return {};
  }
}

const ElmoMotorController::TimeStampedParamMap& ElmoMotorController::parameters()
{
  const DS_D(ElmoMotorController);
  return d->parameters_;
}

void ElmoMotorController::addPreemptCommand(ds_asio::IoCommand command)
{
  DS_D(ElmoMotorController);
  if (d->has_started_)
  {
    command = d->wrap_command_for_logging(std::move(command));
    d->elmo_sm_->addPreemptCommand(std::move(command));
  }
  else
  {
    d->init_commands_.emplace_back(std::move(command));
  }
}

uint64_t ElmoMotorController::addRegularCommand(ds_asio::IoCommand command)
{
  DS_D(ElmoMotorController);
  auto id = std::numeric_limits<uint64_t>::max();
  if (d->has_started_)
  {
    command = d->wrap_command_for_logging(std::move(command));
    id = d->elmo_sm_->addRegularCommand(command);
  }
  else
  {
    command.setId(id);
    d->poll_commands_.emplace_back(std::make_pair(id, std::move(command)));
  }

  return id;
}

const std::list<ElmoMotorController::PollCommand>& ElmoMotorController::polledCommands()
{
  DS_D(ElmoMotorController);
  return d->poll_commands_;
}

bool ElmoMotorController::replaceRegularCommand(uint64_t id, ds_asio::IoCommand command)
{
  DS_D(ElmoMotorController);
  command = d->wrap_command_for_logging(std::move(command));
  return d->elmo_sm_->overwriteRegularCommand(id, std::move(command));
}
}
