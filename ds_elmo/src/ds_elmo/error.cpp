/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_elmo/error.h"

namespace ds_elmo
{
namespace error_code
{
std::string to_string(const int err)
{
  switch (err)
  {
    case BAD_COMMAND:
      return "The interpreter has not understood the command.";
    case BAD_INDEX:
      return "Attempt to access a vectored variable out of its boundaries.";
    case BAD_CHARACTER:
      return "An unrecognized character has been found where a command was expected.";
    case PROGRAM_NOT_RUNNING:
      return "Program is not running. This command requires a running program.";
    case MODE_UNABLE_TO_START:
      return "Mode cannot be started - bad initialization data.";
    case WRITE_ERROR:
      return "Cannot write to flash memory.";
    case COMMAND_UNAVAILABLE:
      return "Command not available in this unit mode.";
    case CANNOT_RESET_COMMS:
      return "UART is busy and cannot reset parameters";
    case EMPTY_ASSIGN:
      return "Missing value for parameter assignment";
    case BAD_COMMAND_FORMAT:
      return "Bad command format.";
    case OPERAND_OUT_OF_RANGE:
      return "Operand out of range.";
    case ZERO_DIVISION:
      return "Zero division.";
    case UNABLE_TO_ASSIGN:
      return "Command cannot be assigned. (command does not take a value)";
    case BAD_OPERATOR:
      return "Bad operator.";
    case CMD_NOT_VALID_WHEN_MOVING:
      return "Command not valid while moving.";
    case MOTION_MODE_NOT_VALID:
      return "Motion mode not valid.";
    case OUT_OF_LIMIT_RANGE:
      return "Out of limit range.";
    case NO_PROGRAM_TO_CONTINUE:
      return "No program to continue.";
    case COMMS_OVERRUN:
      return "Communication error";
    case DUPLICATE_HALL_SENSOR:
      return "Two or more Hall sensors are defined for the same location.";
    case MOTION_START_TIMEOUT:
      return "Motion start specified for the past.";
    case COMMAND_NOT_SUPPORTED:
      return "Command is not supported by this product";
    case NO_SUCH_LABEL:
      return "No such label.";
    case CAN_STATE_MACHINE_FAULT:
      return "CAN state machine fault (object 0x6040 on";
    case SUBROUTINE_ERROR:
      return "Returned error from subroutine.";
    case HOMING_MODE_STOP:
      return "May not use multi-capture homing mode with stop event.";
    case PROGRAM_DOES_NO_EXIST:
      return "User program does not exist. XQ or XC";
    case STACK_OVERFLOW:
      return "Stack overflow.";
    case ONLY_FOR_CURRENT:
      return "Command is applicable only in torque modes UM=1 or TC=2";
    case BAD_DATABASE:
      return "Bad database.";
    case BAD_CONTEXT:
      return "Bad context";
    case ADV_COMMAND_NOT_SUPPORTED:
      return "The product grade does not support this command.";
    case MOTOR_MUST_BE_OFF:
      return "Motor must be off to use this command.";
    case MOTOR_MUST_BE_ON:
      return "Motor must be on to use this command.";
    case BAD_UNIT_MODE:
      return "Bad unit mode.";
    case DATABASE_RESET:
      return "Database reset to factory defaults.";
    case CANNOT_SET_INDEX:
      return "Cannot set the index of an active table. When the ECAM table is active,";
    case DISABLED_BY_SW:
      return "Disabled by SW.";
    case DRIVE_NOT_READY:
      return "Drive not ready.";
    case RECORDER_BUSY:
      return "Recorder is busy.";
    case RECORDER_ERROR:
      return "Recorder usage error.";
    case RECORDER_DATA_INVALID:
      return "Recorder data invalid.";
    case HOMING_BUSY:
      return "Homing is busy.";
    case MUST_BE_EVEN:
      return "Must be even.";
    case SET_POSITION:
      return "Please set position.";
    case BUFFER_TO_LARGE:
      return "Buffer too large.";
    case PROGRAM_OUT_OF_RANGE:
      return "Out of program range.";
    case ECAM_DATA_INCONSISTENT:
      return "ECAM data inconsistent.";
    case PROGRAM_RUNNING:
      return "Program is running.";
    case COMMAND_NOT_FOR_PROGRAM:
      return "CMD not for program.";
    case SYSTEM_NOT_IN_POINT_TO_POINT:
      return "The system is not in point to point mode. A PR (position relative) cannot";
    case CAN_STATE_MACHINE_NOT_READY:
      return "CAN state machine is not ready (object";
    case BAD_INITIATION_VALUE:
      return "There is a wrong initiation value for this";
    case TOO_BIG_FOR_MODULO:
      return "Too large for modulo setting. The modulo range is inconsistent";
    case USER_PROGRAM_TIMEOUT:
      return "User program time out.";
    case RS232_RX_OVERFLOW:
      return "RS232 receive buffer overflow.";
    case AUX_FEEDBACK:
      return "The auxiliary feedback entry does not";
    case PWM_NOT_SUPORTED:
      return "The requested PWM value is not supported";
    case POSITION_READ_ABORT:
      return "Abortive attempt to read a position value";
    case SPEED_KP_RANGE:
      return "Speed loop KP out of range.";
    case POSITION_KP__RANGE:
      return "Position loop KP out of range.";
    case KV_N_INVALID:
      return "KV[N] vector is invalid.";
    case KV_SCHEDULE_OFF:
      return "KV[N] defines scheduled block but";
    case EXP_TASK_QUEUE_FULL:
      return "Exp task queue is full.";
    case EXP_TASK_QUEUE_EMPTY:
      return "Exp task queue is empty.";
    case EXP_OUTPUT_QUEUE_FULL:
      return "Exp output queue is full";
    case EXP_OUTPUT_QUEUE_EMPTY:
      return "Exp output queue is empty.";
    case BAD_KV_SETTING:
      return "Bad KV setting for sensor filter.";
    case BAD_KV_VECTOR:
      return "Bad KV vector This can happen when KV";
    case BAD_ANALOG_SENSOR:
      return "Bad Analog sensor Filter When the filter KV, set for analog";
    case BAD_NUM_ANALOG_BLOCKS:
      return "Bad number of blocks for Analog sensor";
    case ANALOG_SENSOR_NOT_READY:
      return "Analog sensor is not ready When the initiation procedure of";
    case MODULO_NEGATIVE:
      return "Modulo range must be positive.";
    case NOT_AN_ARRAY:
      return "Variable is not an array.";
    case DOES_NOT_EXIST:
      return "Variable name does not exist.";
    case CANNOT_RECORD_LOCAL:
      return "Cannot record local variables.";
    case NOT_AN_ARRAY_INTERNAL:
      return "Variable is not an array.";
    case MUSMATCH_NUM_USER_SYSTEM:
      return "Mismatched number of user/system";
    case CANNOT_RUN_LABEL:
      return "Program already compiled.";
    case TOO_MANY_BREAKPOINTS:
      return "The number of breakpoints exceeds the";
    case FAILED_TO_MODIFY_BREAKPOINT:
      return "An attempt to set/clear breakpoint at the";
    case BOOT_ID_NON_EMPTY:
      return "Boot identity parameters section is not";
    case INVALID_CHECKSUM:
      return "Checksum of data is not correct.";
    case MISSING_BOOT_ID:
      return "Missing boot identity parameters.";
    case NUMERIC_STACK_UNDERFLOW:
      return "Numeric stack underflow.";
    case NUMERIC_STACK_OVERLFLOW:
      return "Numeric stack overflow.";
    case EXPR_STACK_OVERFLOW:
      return "Expression stack overflow.";
    case COMMAND_WITHIN_MATH:
      return "Executable command within math";
    case EMPTY_EXPRESSION:
      return "Nothing in the expression.";
    case UNEXPECTED_EOS:
      return "Unexpected sentence termination.";
    case EXPRESSION_TO_LONG:
      return "Sentence terminator not found.";
    case MISMATCHED_PARENTHESES:
      return "Parentheses mismatch.";
    case BAD_OPERAND_TYPE:
      return "Bad operand type.";
    case ADDR_OUT_OF_MEM_RANGE:
      return "Address is out of data memory segment.";
    case ADDR_OUT_OF_STACK_RANGE:
      return "Beyond stack range.";
    case BAD_OPCODE:
      return "Bad opcode.";
    case NO_PROGRAM_STACK:
      return "No available program stack.";
    case ADDR_OUT_OF_FLASH_RANGE:
      return "Out of flash memory range.";
    case FLASH_VERIFY_ERROR:
      return "Flash verify error.";
    case PROGRAM_TERMINATED:
      return "Program aborted by another threat.";
    case PROGRAM_NOT_HALTED:
      return "Program is not halted.";
    case BAD_NUMBER:
      return "Badly formatted number.";
    case EC_COMMAND:
      return "EC command (not an error).";
    case SERIAL_FLASH_BUSY:
      return "An attempt to access serial flash while busy. Contact Technical Support.";
    case OUT_OF_MODULO_RANGE:
      return "Out of modulo range. XM[1]=-1000, XM[2]=1000 and";
    case INFINITE_LOOP:
      return "Infinite loop in for loop - zero step k=1:0:10; causes this error.";
    case SPEED_TO_LARGE:
      return "Speed too large to start motor.";
    default:
      char errstr[100];
      sprintf(errstr, "Unknown error: %d", err);
      return std::string{ errstr };
  }
}
std::ostream& operator<<(std::ostream& os, const int err)
{
  return os << to_string(err);
}
}
}
