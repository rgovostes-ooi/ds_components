# ds_elmo: A general Elmo motor-controller node for ds_ros

This package provides a general `DsProcess`-based node for operating
an Elmo motor controller.