/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DS_COULOMB_COUNTER_H
#define DS_COULOMB_COUNTER_H

#include "ds_base/ds_process.h"
#include "ds_core_msgs/RawData.h"
#include "ds_hotel_msgs/Charge.h"
#include "ds_base/sensor_base.h"

#include "ds_hotel_msgs/ChargeCmd.h"

#include <ds_base/util.h>

namespace ds_components
{
struct CoulombCounterPrivate;

class CoulombCounter : public ds_base::SensorBase
{
  DS_DECLARE_PRIVATE(CoulombCounter)

public:
  explicit CoulombCounter();
  CoulombCounter(int argc, char* argv[], const std::string& name);
  ~CoulombCounter() override;

  DS_DISABLE_COPY(CoulombCounter);

  // inputs charge data from serial line subscriber and parses to message type
  static std::pair<bool, ds_hotel_msgs::Charge> parse_charge(const ds_core_msgs::RawData& bytes);

  float shunt_1;
  float shunt_2;
  float shunt_3;
  float shunt_4;

  int frequency;

protected:
  void setupConnections() override;
  void setupPublishers() override;
  void setupParameters() override;
  void setupServices() override;

  // pulls parsed charge messages from parse_charge and publishes to topic
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;

  bool send_cmds(ds_hotel_msgs::ChargeCmd::Request& req, ds_hotel_msgs::ChargeCmd::Response& resp);

private:
  std::unique_ptr<CoulombCounterPrivate> d_ptr_;

  boost::shared_ptr<ds_asio::DsConnection> board_connection;

  // for service advertisement
  ros::ServiceServer cmd_service;
};
}

#endif
