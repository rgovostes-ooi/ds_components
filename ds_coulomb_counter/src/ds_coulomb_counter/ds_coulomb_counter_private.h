
#ifndef DS_COULOMB_COUNTER_PRIVATE_H
#define DS_COULOMB_COUNTER_PRIVATE_H

#include "ds_hotel_msgs/Charge.h"
#include "ds_coulomb_counter.h"

namespace ds_components
{
struct CoulombCounterPrivate
{
  CoulombCounterPrivate() : address_command_(1.0)
  {
  }

  // publisher
  ros::Publisher charge_pub_;

  // I/O state machine
  boost::shared_ptr<ds_asio::IoSM> iosm;

  ds_asio::IoCommand address_command_;

  // address
  int address_;

  // represents interval between samples, NOT frequency
  int sampling_rate_;

  // shunt resistor values in milliohms
  float shunt_ch1_;
  float shunt_ch2_;
  float shunt_ch3_;
  float shunt_ch4_;

  bool reset_ch1_;
  bool reset_ch2_;
  bool reset_ch3_;
  bool reset_ch4_;
};
}

#endif  // DS_COULOMB_COUNTER_PRIVATE_H
