# Sentry's Abort Supervisor

This node is only a place holder for the moment.  The Sentry gazebo
sim requires a ds_core_msgs::Abort message published on /[vehiclename]/abort
to function.  This node accomplishes that task and not much else.
